<?php

class Utility_Unicode
{
	public static function extractNumbers($string)
    {
		return preg_replace("/[^0-9]/", '', $string);
	}

	public static function readMoreText($str, $count = 50)
    {
		$tmp = explode(' ', $str);
		if (count($tmp) <= $count) return $str;
		$tmp = array_slice($tmp, 0, $count);
		return implode(' ', $tmp) . '...';
	}

    public static function duration2text($dur)
    {
        if ($dur < 60) return $dur . ' giây trước';
        if ($dur < 3600) return  intval($dur / 60) . ' phút trước';
        if ($dur < 86400) return  intval($dur / 3600) . ' giờ trước';
        if ($dur < 604800) return  intval($dur / 86400) . ' ngày trước';
        if ($dur < 2592000) return  intval($dur / 604800) . ' tuần trước';
        if ($dur < 31104000) return  intval($dur / 2592000) . ' tháng trước';
        return date('d/m/Y H:i', time() - $dur);
    }

    public static function timestamp2text($ts)
    {
        $dow = array('chủ nhật', 'thứ hai', 'thứ ba', 'thứ tư', 'thứ năm', 'thứ sáu', 'thứ bảy');
        return sprintf('Cập nhật %s, ngày %s', $dow[date('w', $ts)], date('d/m/y H:i', $ts));
    }

    public static function date2text($date)
    {
        return self::timestamp2text(strtotime($date));
    }

    public static function toPermalink($title)
    {
        return self::toSlug($title);
    }

    public static function toSlug($title)
    {
        /*
         * Replace with "-"
         * Change it if you want
         */

        $replacement = '-';
        $map = array();
        $quotedReplacement = preg_quote($replacement, '/');

        $default = array(
                '/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ|À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ|å/' => 'a',
                '/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ|È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ|ë/' => 'e',
                '/ì|í|ị|ỉ|ĩ|Ì|Í|Ị|Ỉ|Ĩ|î/' => 'i',
                '/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ|Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ|ø/' => 'o',
                '/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ|Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ|ů|û/' => 'u',
                '/ỳ|ý|ỵ|ỷ|ỹ|Ỳ|Ý|Ỵ|Ỷ|Ỹ/' => 'y',
                '/đ|Đ/' => 'd',
                '/ç/' => 'c',
                '/ñ/' => 'n',
                '/ä|æ/' => 'ae',
                '/ö/' => 'oe',
                '/ü/' => 'ue',
                '/Ä/' => 'Ae',
                '/Ü/' => 'Ue',
                '/Ö/' => 'Oe',
                '/ß/' => 'ss',
                '/[^\s\p{Ll}\p{Lm}\p{Lo}\p{Lt}\p{Lu}\p{Nd}]/mu' => ' ',
                '/\\s+/' => $replacement,
                sprintf('/^[%s]+|[%s]+$/', $quotedReplacement, $quotedReplacement) => '',
            );
        //Some URL was encode, decode first
        $title = urldecode($title);

        $map = array_merge($map, $default);
        return strtolower( preg_replace(array_keys($map), array_values($map), $title) );
    }

    static function converUrl($str){
        if(!$str) return false;

        $str = self::get_utf8_to_ascii($str);
        $unicode = array('-' => ' ');

        foreach($unicode as $nonUnicode=>$uni)
            $str = preg_replace("/($uni)/i",$nonUnicode,$str);
        return $str;
    }

    static function get_utf8_to_ascii($str){
        if(!$str) return false;
        $unicode = array('a' => 'A|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ằ|Ẳ|Ẵ|Ặ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ|á|à|ả|ã|ạ|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ',
                        'b' => 'B',
                        'c' => 'C',
                        'd' => 'D|Đ|đ',
                        'e' => 'E|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ|é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
                        'f' => 'F',
                        'g' => 'G',
                        'h' => 'H',
                        'i' => 'I|Í|Ì|Ỉ|Ĩ|Ị|í|ì|ỉ|ĩ|ị',
                        'j' => 'J',
                        'k' => 'K',
                        'l' => 'L',
                        'm' => 'M',
                        'n' => 'N',
                        'o' => 'O|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ|ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
                        'p' => 'P',
                        'q' => 'Q',
                        'r' => 'R',
                        's' => 'S',
                        't' => 'T',
                        'u' => 'U|Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự|ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
                        'v' => 'V',
                        'w' => 'W',
                        'x' => 'X',
                        'y' => 'Y|Ý|Ỳ|Ỷ|Ỹ|Ỵ|ý|ỳ|ỷ|ỹ|ỵ',
                        'z' => 'Z',
                        '' => '&',
                        '' => "'");
        foreach($unicode as $nonUnicode=>$uni)
            $str = preg_replace("/($uni)/i",$nonUnicode,$str);
        return $str;
    }


    static function get_str_replace($str){
        $str = self::get_utf8_to_ascii(trim($str));
        $str = trim(preg_replace('~[^a-z0-9]+~', ' ', $str));
        return preg_replace('~[^a-z0-9]+~', '-', $str);
    }
    static function convertLink($typeId,$id,$slug){
        switch ($typeId) {
            case 0:
                $path = 'du-an'; break;
            case 1:
                $path = 'tin-tuc'; break;
            case 2:
                $path = 'gioi-thieu';break;
            case 3:
                $path = 'lien-he';break;
            default:
                $path = ''; break;
        }
      return '/'.$path.'/'.$id.'-'.$slug.'.html';
    }
    static function getPath($url){
        $url = str_replace('http://','',$url);
        $arr = explode('/',$url);
        $url = str_replace($arr[0],'',$url);
        return $url;
    }
}
