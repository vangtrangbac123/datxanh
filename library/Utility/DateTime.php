<?php
class Utility_DateTime {

	// create invoice transaction
	static function getInvoiceNo(){
		try {

			$time = explode(" ", microtime());
			$time= substr($time[0], 2, 6);
			$time = date('YmdHis') . $time;

			return $time;
		} catch (Zend_Exception $e) {
			throw $e;
		}
	}

	static function getMicrotime(){
		try {

			$time = explode(" ", microtime());
			$time= substr($time[0], 2, 6);
			$time = date('YmdHis') . $time;

			return $time;
		} catch (Zend_Exception $e) {
			throw $e;
		}
	}

	static function getMicrotimePushERP(){
		try {

			$time = explode(" ", microtime());
			$time= substr($time[0], 2, 6);
			$time = date('Y-m-d H:i:s') . ".". $time;

			return $time;
		} catch (Zend_Exception $e) {
			throw $e;
		}
	}

	static function microtime(){
		try {

			$time = microtime(true);

			return $time;
		} catch (Zend_Exception $e) {
			throw $e;
		}
	}
	public static function timeUntil($seconds)
    {
        $seconds = (int)$seconds;



        $hours = sprintf("%02s",floor($seconds / 3600));
        $seconds %= 3600;

        $minutes = sprintf("%02s",floor($seconds / 60));
        $seconds %= 60;

        $s = array('ngày', 'giờ', 'phút', 'giây');

        if ($hours > 0)     return   $hours.':'.$minutes.':'.sprintf("%02s",$seconds);
        if ($minutes ==  0) return   sprintf("%02s",$seconds).'s';
        if ($hours == 0 )    return   $minutes.':'.sprintf("%02s",$seconds);

    }


}