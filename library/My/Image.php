<?php

class My_Image {

    private static $_staticBaseUrl = PUBLIC_PATH_UPLOAD_IMAGES_PRODUCTS;

    /*
     * Size Product
    */
    public static $PRODUCT_IMAGE_SIZE_50x50 = array('height' => 50, 'width' => 50, 'sizename' => '50x50');
    public static $PRODUCT_IMAGE_SIZE_149x149 = array('height' => 149, 'width' => 149, 'sizename' => '149x149');
    public static $PRODUCT_IMAGE_SIZE_100x100 = array('height' => 100, 'width' => 100, 'sizename' => '100x100');
    public static $PRODUCT_IMAGE_SIZE_155x255 = array('height' => 255, 'width' => 155, 'sizename' => '155x255');

    /* Shop Logo*/
    public static $SHOPLOGO_IMAGE_SIZE_60x60 = array('height' => 60, 'width' => 60, 'sizename' => '60x60');

    static public function setStaticURL($baseUrl) {
        self::$_staticBaseUrl = $baseUrl;
    }

    static public function resize($size = array(), $fileName = null, $type, $url) {


        $path = $fileName . $type;

        //if (isset(self::$_staticBaseUrl) && isset($path)) {



        if (!empty($url)) {

            $image = Plugins_Image::factory();
            $image->open($url);

            $h = $image->getHeight();
            $w = $image->getWidth();

            $newWidth = null;
            $newHeight = null;
            $rate = $h / $w;

            //resize theo tỉ lệ khung, resize lệ theo cạnh nhỏ hơn. căn chỉnh theo center cạnh lớn.
            if ($h > $w) {
                //hình theo chiều đứng, resize theo theo chiều đứng,
                $newWidth = $size['width'] > $w ? $w : $size['width'];
                $newHeight = $newWidth * $rate;

                if($newHeight <= $size['height']){
                    $newHeight = $size['height'];
                    $newWidth = $newHeight / $rate;
                }

            } else {
                //hình theo chiều ngang, resize theo theo chiều ngang,
                $newHeight = $size['height'] > $h ? $h : $size['height'];
                $newWidth = $newHeight / $rate;

                if($newWidth <= $size['width']){
                    $newWidth = $size['width'];
                    $newHeight = $newWidth * $rate;
                }
            }


            $image->resize($newWidth, $newHeight);
            list($x, $y) = self::getCenter($size['width'], $size['height'], $image);
            $image->crop($x, $y, $size['width'], $size['height']);


            var_dump($image);die;

            $image->write(self::$_staticBaseUrl . DS . $fileName . '_' . $size['sizename'] . $type);

            die;

            return ($fileName . '_' . $size['sizename'] . $type);
        }
    }

    static private function getCenter($w, $h, $image) {

        $cx = $image->getWidth() / 2;
        $cy = $image->getHeight() / 2;
        $x = (int) (($cx - $w / 2) > 0 ? ($cx - $w / 2) : 0);
        $y = (int) (($cy - $h / 2) > 0 ? ($cy - $h / 2) : 0);

        return array($x, $y);
    }


    static public function getImage($imageUrl, $params){

        $_params = "";

        foreach ($params as $key => $val) {
            $_params .= '&' . $key . '=' . $val;
        }

        $url = IMAGE_HOST . '?url=' . $imageUrl . $_params;

        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);

        $data = Zend_Json::decode($data);

        if ($data['error'] == 0){
            $imageUrl = $data['src'];
        }

        return $imageUrl;

    }
    public static function requestResize($params) {

        try {

            if (!isset($params['url'])) return false;

            $config = App::getConfig('proxy');
            $url = 'http://upload.123phim.vn/go123/resize/?'.http_build_query($params);
            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT,        60);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($ch, CURLOPT_PROXY,          $config['host']);
            curl_setopt($ch, CURLOPT_PROXYPORT,      $config['port']);

            $data = curl_exec($ch);
            curl_close($ch);

            if (empty($data)) return $imageUrl;

            $data = json_decode($data, true);

            if (is_array($data) && isset($data['src']) && !empty($data['src'])
                && isset($data['error']) && $data['error'] == 0) {

                return $data['src'];
            }
        } catch (Zend_Exception $e) {
            // TODO: write log
        }

        return $params['url'];

    }


    public static function resizeOplus($imageUrl, $width, $height, $quality = 70, $crop = 1){
        return self::requestResize(array(
            'url'     => $imageUrl,
            'width'   => $width,
            'height'  => $height,
            'quality' => $quality,
            'crop'    => $crop,
        ));
    }

    public static function ranger($url){
        $config = App::getConfig('proxy_alt');
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_PROXY,          $config['host']);
        curl_setopt($ch, CURLOPT_PROXYPORT,      $config['port']);
        $result = curl_exec($ch);
        $img = imagecreatefromstring($result);
        return $img;
    }

}