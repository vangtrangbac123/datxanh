<?php

class My_Registry {

    protected static $_instance;
    protected static $_data = array();
    private $model;


    static function getInstance() {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
        

    function __construct() {
        $this->model = My_Model_Helperweb::getInstance();
    }
    function getRouter() {
        $router = new Zend_Controller_Router_Rewrite();
        $router->addRoutes(require APPLICATION_PATH . '/configs/routes.php');
        return $router;
    }
     function getListCategory() {
        $sql = "SELECT * FROM categories WHERE  is_active = 1 ORDER BY category_id ";
        $category =  $this->model->Category->getRows($sql);
        foreach ($category as $key => $cat) {
            $cat->list_product = $this->model->Product->getListProductByCat($cat->category_id);
        }
        return $category;
    }

    public function getListMenu(){
        $sql ="SELECT
                    menu_id,
                    menu_name,
                    url
                    FROM menu
                    WHERE is_active = 1
                    ORDER BY number
                    ";
        return $this->model->Menu->getRows($sql);
    }


    function getListEvent(){
        $sql = "SELECT date_show date,'meeting' type,event_title title, event_description description FROM event WHERE  is_active = 1 ORDER BY date_add DESC";
       return $this->model->Event->getRows($sql);
    }
    function getListShowing(){
       $sql = "SELECT   p.product_id,
                        p.product_name,
                        p.url_short,
                        p.product_slug,
                        i.size2 image
               FROM product p
               LEFT JOIN product_image pi ON pi.product_id = p.product_id AND pi.type_id = 1
               LEFT JOIN images i ON i.image_id = pi.image_id
               JOIN categories c ON c.category_id = p.type_id
               WHERE  p.is_active = 1 AND p.status_id = 2
               ORDER BY p.is_hot, p.date_add DESC
               LIMIT 10";
        return $this->model->Product->getRows($sql);
    }
    function getListNews(){
        $sql = "SELECT  *
               FROM news
               WHERE  is_active = 1 AND type_id = 1 
               ORDER BY is_hot, date_add DESC
               LIMIT 10";
        return $this->model->News->getRows($sql);
    }
    function domain(){
      if(APPLICATION_ENV == 'local'){
        return 'opalriversides-local.com';
      }
      return 'datxanhgroup.com';
    }
}