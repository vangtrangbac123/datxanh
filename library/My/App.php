<?php

class My_App {

    protected static $_instance;
    protected static $_configs = array();
    private static $ts = array();
    private static $isDebug;

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public static function timeStart($t = 0)
    {
        self::$ts[$t] = microtime(true);
    }

    public static function timeEnd($t = 0, $callback = null)
    {
        $t2 = microtime(true);
        $tx = round(($t2 - self::$ts[$t]) * 1000, 2);
        if (is_callable($callback)) {
            call_user_func($callback, $tx);
        } elseif (self::isLocal()) {
            error_log("time #$t: $tx     " . self::$requestPath);
        }
    }

    public static function isLocal()
    {
        return (APPLICATION_ENV == 'local');
    }

    public static function isDevelopment()
    {
        return (APPLICATION_ENV == 'development');
    }

    public static function isProduction()
    {
        return (APPLICATION_ENV == 'production');
    }

    public static function isDebug()
    {
        if (!isset(self::$isDebug)) {

            $flag = 0;

            if (isset($_GET['debug'])) {
                $flag = $_GET['debug'];
            } elseif (isset($_COOKIE['debug'])) {
                $flag = $_COOKIE['debug'];
            }

            self::$isDebug = (self::isProduction() ? (string)$flag === 'tpfdev' : (int)$flag === 1);
        }

        return self::$isDebug;
    }

    public static function getConfig($name = null, $env = APPLICATION_ENV) {

        if (!isset(self::$_configs[$env])) {

            $config = false;
            $file1 = CONFIG_PATH . DIRECTORY_SEPARATOR . 'config.php';
            $file2 = CONFIG_PATH . DIRECTORY_SEPARATOR . "config-$env.php";

            if (file_exists($file1)) {
                require $file1;
            } else {
                throw new Exception("Config not found: $file1");
            }

            if (file_exists($file2)) {
                require $file2;
            }

            self::$_configs[$env] = $config;
        }

        if ($name !== null && isset(self::$_configs[$env][$name])) {
            return self::$_configs[$env][$name];
        }

        return self::$_configs[$env];
    }

    public static function getClientIp() {

        if (isset($_SERVER['HTTP_CLIENT_IP']))
            return $_SERVER['HTTP_CLIENT_IP'];

        if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            return $_SERVER['HTTP_X_FORWARDED_FOR'];

        if (isset($_SERVER['HTTP_X_FORWARDED']))
            return $_SERVER['HTTP_X_FORWARDED'];

        if (isset($_SERVER['HTTP_FORWARDED_FOR']))
            return $_SERVER['HTTP_FORWARDED_FOR'];

        if (isset($_SERVER['HTTP_FORWARDED']))
            return $_SERVER['HTTP_FORWARDED'];

        if (isset($_SERVER['REMOTE_ADDR']))
            return $_SERVER['REMOTE_ADDR'];

        return 'UNKNOWN';
    }

}
