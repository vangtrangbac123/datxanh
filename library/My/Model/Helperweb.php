<?php

class My_Model_Helperweb {

    protected static $_instance;
    private $prefix = 'Web_Model_';
    private $models = array();
    private $hasAdapter = false;

    public function __construct($prefix = null) {
        if ($prefix !== null) {
            $this->prefix = $prefix;
        }
    }

    public function __get($name) {
        $class = $this->prefix . $name;
        if (!isset($this->models[$class])) {
            if (!$this->hasAdapter) {
                $this->setupAdapter();
            }
            $this->models[$class] = new $class;
        }
        return $this->models[$class];
    }

    public function setupAdapter() {
        $db = Zend_Db_Table::getDefaultAdapter();
        if ($db) {
            $this->hasAdapter = true;
        } else {
            $config = require APPLICATION_PATH . '/configs/application.php';
            if (isset($config['resources'])
                && isset($config['resources']['db'])
                && isset($config['resources']['db']['adapter'])
                && isset($config['resources']['db']['adapter'])
            ) {
                $db = Zend_Db::factory($config['resources']['db']['adapter'], $config['resources']['db']['params']);
                Zend_Db_Table::setDefaultAdapter($db);
                $this->hasAdapter = true;
            } else {
                throw new Exception('Db config not found');
            }
        }
    }

    public function setPrefix($prefix) {
        $this->prefix = $prefix;
    }

    public static function getInstance() {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
}