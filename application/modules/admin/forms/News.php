<?php

$fields = array();
$sort   = array();
$sort['is_active'] = array(
    'label' => 'Active',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => 'sort',
    'list' => Admin_Model_Form::getListActive(),
    'key'  => 'is_active'
);



$fields['is_active'] = array(
    'label' => 'Active',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);



$fields['is_hot'] = array(
    'label' => 'Hot',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);

$fields['news_title'] = array(
    'label' => 'Title',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['news_description'] = array(
    'label' => 'Description',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);



$fields['news_tags'] = array(
    'label' => 'Tags',
    'data'  => Admin_Model_Form::DATA_STRING,
    'type'  => Admin_Model_Form::TYPE_TAGS_INPUT,
);



$fields['number'] = array(
    'label' => 'Number',
    'data'  => Admin_Model_Form::DATA_STRING,
    'type'  => Admin_Model_Form::TYPE_TEXTBOX
);


$fields['@image_1'] = array(
    'label' => '300x200',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_IMAGE,
    'image' => array(
        array(
            'key'    => 'image_1_2',
            'width'  => 300,
            'height' => 200,
            'type'   => 1,
            'size'   => 2,
        )
    )
);

$fields['content'] = array(
    'label' => 'Content',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTEDITOR2
);

$fields['meta_title'] = array(
    'label' => 'Meta Title',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);

$fields['meta_keyword'] = array(
    'label' => 'Meta Keyword',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);


$fields['meta_description'] = array(
    'label' => 'Meta Description',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);


$fields['og_title'] = array(
    'label' => 'Og title',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);


$fields['og_description'] = array(
    'label' => 'Og Description',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);



$fields['og_url'] = array(
    'label' => 'Og url',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['og_image'] = array(
    'label' => 'Og image',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);






$listview = array(
    'part' => 'news',
    'colums' => array('#', 'Image', 'Title',  'Date', 'Active', 'Action')
);

$list = array();
$list['model'] = 'News';
$list['form']  = 'News';
$list['table'] = 'news';
$list['primary'] = 'news_id';
$list['fields'] = $fields;
$list['listview'] = $listview;
$list['sort'] = $sort;

return $list;