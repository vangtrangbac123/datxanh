<?php

$fields = array();

$fields['is_active'] = array(
    'label' => 'Active',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);

$fields['name'] = array(
    'label' => 'Name',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['email'] = array(
    'label' => 'Email',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);


$fields['phone'] = array(
    'label' => 'phone',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);


$fields['address'] = array(
    'label' => 'Address',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['logo'] = array(
    'label' => 'Logo',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);


$fields['banner_header'] = array(
    'label' => 'Banner Header (750x120)',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);



$listview = array(
    'part' => 'menu',
    'colums' => array('#', 'Menu', 'Link', 'Number','Active', 'Action')
);

$list = array();
$list['model'] = 'Config';
$list['form']  = 'Config';
$list['table'] = 'config';
$list['primary'] = 'config_id';
$list['fields'] = $fields;
$list['listview'] = $listview;

return $list;