<?php

$fields = array();

$fields['is_active'] = array(
    'label' => 'Active',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);

$fields['page'] = array(
    'label' => 'Page (path ex: /tin-tuc)',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['meta_title'] = array(
    'label' => 'Meta title',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);


$fields['meta_keyword'] = array(
    'label' => 'Meta Keyword',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);


$fields['meta_description'] = array(
    'label' => 'Meta Description',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);




$fields['og_title'] = array(
    'label' => 'Og title',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);


$fields['og_description'] = array(
    'label' => 'Og Description',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);



$fields['og_url'] = array(
    'label' => 'Og url',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['og_image'] = array(
    'label' => 'Og image',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);






$listview = array(
    'part' => 'seo',
    'colums' => array('#', 'Meta', 'page', 'Active', 'Action')
);

$list = array();
$list['model'] = 'Seo';
$list['form']  = 'Seo';
$list['table'] = 'seo';
$list['primary'] = 'meta_id';
$list['fields'] = $fields;
$list['listview'] = $listview;

return $list;