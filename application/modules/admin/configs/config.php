<?php

$config = array();

$config['application'] = array(
    // 'appnamespace' => '',
    'bootstrap' => array(
        'path' => APPLICATION_PATH . '/Bootstrap.php',
        'class' => 'Bootstrap'
    ),
    'resources' => array(
        'layout' => array(
        ),
        'modules' => array(),
        'frontController' => array(
            'baseUrl' => '',
            'defaultModule' => 'admin',
            'moduleDirectory' => APPLICATION_PATH . '/modules',
            'prefixDefaultModule' => true,
            'throwExceptions' => false,
            'params' => array(
                'displayExceptions' => true,
                'noViewRenderer' => false,
            ),
            'plugins' => array(
                'Route' => 'Plugin_Route'
            ),
        ),
        'db' => array(
            'adapter' => 'Pdo_Mysql',
            'params' => array(
                'charset'  => 'utf8',
                'host'     => 'localhost',
                'dbname'   => 'customer_web',
                'username' => 'root',
                'password' => '',
                'profiler' => true,
                'options'  => array(
                    'fetchMode' => 5 // Zend_Db::FETCH_OBJ
                ),
            ),
        ),
    ),
);

$config['proxy'] = array(
    'host' => null,
    'port' => null,
);

$config['proxy_alt'] = array(
    'host' => null, // 10.30.15.68:8888
    'port' => null,
);


$config['memcache'] = array(
    'host'   => '10.40.52.11',
    'port'   => '11211',
    'prefix' => 'app',
    'enable' => true,
);

$config['zingtv'] = array(
    'url'  => 'http://api.tv.zing.vn/2.0/movie/episode/info',
    'key'  => '65517cf4203efa9a822186a01164c7cf',
);

