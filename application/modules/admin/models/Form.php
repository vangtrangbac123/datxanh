<?php

class Admin_Model_Form {

    const DATA_INT    = 0;
    const DATA_STRING = 1;
    const DATA_DATE   = 2;
    const DATA_ARRAY  = 3;

    const TYPE_TEXTBOX       = 'textbox';
    const TYPE_TEXTAREA      = 'textarea';
    const TYPE_TEXTBOXREAD   = 'textboxread';
    const TYPE_TEXTEDITOR    = 'texteditor';
    const TYPE_TEXTEDITOR2   = 'texteditor2';
    const TYPE_DATEBOX       = 'datebox';
    const TYPE_SELECT        = 'select';
    const TYPE_SELECT_MULTI  = 'selectmulti';
    const TYPE_CHECKBOX      = 'checkbox';
    const TYPE_IMAGE         = 'image';
    const TYPE_GALLERY       = 'gallery';
    const TYPE_TAGS_INPUT    = 'tagsinput';
    const TYPE_FILM_ACTORS   = 'filmactors';
    const TYPE_FILM_DIRECTOR = 'filmdirector';
    const TYPE_MULTIDATE     = 'multidate';

    public static function get($name) {
        $path = APPLICATION_PATH . '/modules/admin/forms/' . ucfirst($name) . '.php';
        if (!file_exists($path)) return false;
        return include $path;
    }

    public static function getListGroup() {
        $model = My_Model_Helper::getInstance();
        $rows = $model->Location->getRows('SELECT p_cinema_id, p_cinema_name FROM p_cinema ORDER BY number, p_cinema_id');
        $list = array();
        $list[''] = 'Choose a Group';
        foreach ($rows as $row) {
            $list[$row->p_cinema_id] = $row->p_cinema_name;
        }
        return $list;
    }

    public static function getListCinema() {
        $model = My_Model_Helper::getInstance();
        $rows = $model->Cinema->getRows('SELECT cinema_id, cinema_name FROM cinemas where is_active = 1 ORDER BY p_cinema_id,cinema_id');
        $list = array();
        $list[''] = 'Choose a Cinema';
        foreach ($rows as $row) {
            $list[$row->cinema_id] = $row->cinema_name;
        }
        return $list;
    }



    public static function getListLocation() {
        $model = My_Model_Helper::getInstance();
        $rows = $model->Location->getRows('SELECT location_id, city_name FROM locations ORDER BY number, location_id');
        $list = array();
        $list[''] = 'Choose a Location';
        foreach ($rows as $row) {
            $list[$row->location_id] = $row->city_name;
        }
        return $list;
    }

    public static function getListPositionBanner() {
        $model = My_Model_Helper::getInstance();
        $list = array();
        $list['']   = 'Choose a Type';
        $list['1'] = 'Banner Slide (1600 x 518)';
        $list['2'] = 'Banner SideBar (218 x 140)';
        return $list;
    }

    public static function getListTypeNews() {
        $model = My_Model_Helper::getInstance();
        $rows = $model->Menu->getRows("SELECT menu_slug, menu_name FROM menu WHERE is_active = 1  ORDER BY number ");
        $list = array();
        $list[''] = 'Choose a Type';
        foreach ($rows as $row) {
            $list[$row->menu_slug] = $row->menu_name;
        }
        return $list;
    }

    public static function getListUser() {
        $model = My_Model_Helper::getInstance();
        $rows = $model->User->getRows('SELECT user_id, username FROM users ORDER BY user_id');
        $list = array();
        $list[''] = 'Choose a User';
        foreach ($rows as $row) {
            $list[$row->user_id] = $row->username;
        }
        return $list;
    }

     public static function getListBanner() {
        $model = My_Model_Helper::getInstance();
        $rows = $model->User->getRows('SELECT banner_id, banner_title FROM banner_mobile ORDER BY banner_id');
        $list = array();
        $list[''] = 'Choose a Banner';
        foreach ($rows as $row) {
            $list[$row->banner_id] = $row->banner_title;
        }
        return $list;
    }
    public static function getListGallery() {
        $model = My_Model_Helper::getInstance();
        $rows = $model->Gallery->getRows('SELECT gallery_id, gallery_name FROM gallery ORDER BY gallery_id');
        $list = array();
        $list[''] = 'Choose a Gallery';
        foreach ($rows as $row) {
            $list[$row->gallery_id] = $row->gallery_name;
        }
        return $list;
    }

    public static function getListGroupUser() {
        $list = array();
        $list['0']  = 'All';
        $list['1']  = 'A1';
        $list['3']  = 'A3';
        $list['7']  = 'A7';
        $list['30'] = 'A30';
        return $list;
    }

    public static function getListDevice() {
        $list = array();
        $list['0']  = 'Choose a Device';
        $list['1']  = 'iOS';
        $list['2']  = 'Android';
        $list['3']  = 'Window Phone';
        return $list;
    }

    public static function getTypeCinemaPromotion() {
        $list = array();
        $list['0']  = 'Choose a Type';
        $list['1']  = '123phim_all_cinema';
        $list['2']  = '123phim_list_cinema';
        $list['3']  = 'all_cinema';
        $list['4']  = 'list_cinema';
        return $list;
    }

    public static function getListNotificationStatus() {
        $list = array();
        $list['0']  = 'Choose a Status';
        $list['0']  = 'New';
        $list['1']  = 'Success';
        $list['2']  = 'Fail';
        return $list;
    }

    public static function getListCategory() {
        $list = array();
        $list['']   = 'Choose Category';
        $list['1']  = 'News';
        $list['4']  = 'Promotion';
        $list['5']  = 'Cinema';
        $list['6']  = 'Introduction';
        $list['7']  = 'Movie Tickets';
        $list['8']  = 'Reviews';
        $list['9']  = 'Discussion';
        $list['10'] = 'Event';
        return $list;
    }

    public static function getListReview() {
        $list = array();
        $list['']   = 'Choose ReView';
        $list['1']  = '123Phim';
        $list['2']  = 'Cộng đồng';
        $list['3']  = 'Báo chí';
        return $list;
    }

     public static function getListActive() {
        $list = array();
        $list[-1]   = 'Choose Active';
        $list['0']  = 'Inactive';
        $list['1']  = 'Active';
        return $list;
    }

    public static function getListSiteType() {
        $list = array();
        $list['']   = 'Choose a Type';
        $list['0']  = 'WEB';
        $list['1']  = 'IOS';
        $list['2']  = 'Android';
        return $list;
    }

    public static function getListProduct($past = 0) {
        $model = My_Model_Helper::getInstance();
        $sql ='SELECT product_id, product_name FROM product  WHERE status_id in (1,2) AND is_active = 1 ORDER BY status_id ASC ,date_add DESC';
        if($past == 3){
           $sql ='SELECT product_id, product_name FROM product  WHERE status_id in (1,2,3) AND is_active = 1 ORDER BY status_id ASC ,date_add DESC';
        }

        $rows = $model->Product->getRows($sql);
        $list = array();
        $list[''] = 'Choose a product';
        foreach ($rows as $row) {
            $list[$row->product_id] = $row->product_name;
        }
        return $list;
    }

    public static function getListBookingType() {
        $list = array();
        $list[''] = 'Choose a Booking Type';
        $list['-1'] = 'Don\'t support';
        $list['0'] = 'Booking';
        $list['1'] = 'Booking &amp; Payment';
        return $list;
    }

    public static function getListProductStatus() {
        $list = array();
        $list['0'] = 'Choose a Status';
        $list['2'] = 'Now Selling';
        $list['1'] = 'Coming Soon';
        $list['3'] = 'Sold';
        return $list;
    }

    public static function getListProductType() {
        $model = My_Model_Helper::getInstance();
        $sql ='SELECT category_id, category_name FROM categories  WHERE  is_active = 1 ORDER BY category_id ASC';

        $rows = $model->Category->getRows($sql);
        $list = array();
        $list[''] = 'Choose a category';
        foreach ($rows as $row) {
            $list[$row->category_id] = $row->category_name;
        }
        return $list;
    }

    public static function getListFilmPosterRegion() {
        $list = array();
        $list['0'] = 'Choose a Region';
        $list['1'] = 'Region 1';
        $list['2'] = 'Region 2';
        $list['3'] = 'Region 3';
        $list['4'] = 'Region 4';
        return $list;
    }

    public static function getListBankType() {
        $list = array();
        $list['0'] = 'ATM';
        $list['1'] = 'Credit';
        $list['2'] = 'MOMO';
        return $list;
    }

    public static function getListCampaignType() {
        $list = array();
        $list['0'] = 'Choose a Type';
        $list['1'] = 'Áp dụng cho CỤM RẠP';
        $list['2'] = 'Áp dụng cho RẠP';
        $list['3'] = 'Áp dụng cho PHIM';
        $list['11'] = 'Chương trình Agoda';
        return $list;
    }

}