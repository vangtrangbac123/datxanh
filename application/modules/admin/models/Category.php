<?php
class Admin_Model_Category extends My_Model_Abstract {

    protected $_name = 'categories';
    protected $_primary = 'category_id';

    public function getCatBySlug($slug) {
        $sql = 'SELECT * FROM categories WHERE category_slug = ?';
        return $this->_db->fetchRow($sql, array($slug));
    }

    public function getCatByName($name) {
        $sql = 'SELECT * FROM categories WHERE category_name = ?';
        return $this->_db->fetchRow($sql, array($name));
    }

    public function checkCat($name) {
        $row = $this->getCatByName($name);
        if ($row) return $row;

        $slug = Utility_Unicode::get_str_replace($name);

        $row = $this->getCatBySlug($slug);
        if ($row) return $row;

        $catId = (int)$this->save(array(
            'category_slug' => $slug,
            'category_name' => $name,
            'date_add' => date('Y-m-d H:i:s'),
        ));

        return $this->get($catId);
    }
}