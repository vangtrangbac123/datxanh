<?php

class Admin_Model_Dashboard extends My_Model_Abstract {

    protected $_name    = 'devices';
    protected $_primary = 'device_id';

    public function getListInstalledByMonth() {
        $sql = "SELECT
                    type_id, DATE_FORMAT(date_add, '%Y-%m') month,
                    DATE_FORMAT(date_add, '%m/%Y') label,
                    COUNT(*) total
                FROM devices
                GROUP BY month, type_id";
        return $this->_db->fetchAll($sql);
    }

    public function getListTransaction() {
        $sql = "SELECT
                    DATE_FORMAT(date_add, '%d/%m') label,
                    DATE_FORMAT(date_add, '%m-%d') month,
                    COUNT(*) total,
                    SUM(total_item) total_seats,
                    SUM(price_after) total_revenue
                FROM transactions
                WHERE DATE(date_add) >= DATE_SUB(NOW(), INTERVAL 20 DAY)
                    AND status_id = 4 AND payment_status = 1 AND confirm_seat = 1 AND cancel = 0
                GROUP BY month
                ORDER BY month";
        return $this->_db->fetchAll($sql);
    }

    public function getReportData($type) {

        $key = sprintf('admin.report.%s', $type);
        $data = $this->memcache->get($key);
        if ($data) return $data;

        switch ($type) {
            case 1: // group by cinema
                $sql = "SELECT
                            p.p_cinema_name,
                            DATE_FORMAT(t.date_add, '%Y-%m') date,
                            COUNT(*) total,
                            SUM(t.total_item) total_ticket
                        FROM transactions t
                        JOIN p_cinema p ON p.p_cinema_id = t.p_cinema_id
                        WHERE t.status_id = 4 AND t.confirm_seat = 1
                        GROUP BY t.p_cinema_id, date
                        ORDER BY t.p_cinema_id, date DESC";
                break;

            case 2: // group by Web, Mdot, iOS, Android, MoMo
                $sql = "SELECT
                            t0.type_group, t0.date, t0.total,
                            IFNULL(t1.total, 0) total_success,
                            IFNULL(t2.total, 0) total_fail,
                            IFNULL(t3.total, 0) total_cancel
                        FROM (
                            SELECT s.type_group, DATE_FORMAT(t.date_add, '%Y-%m') date, COUNT(*) total
                            FROM transactions t
                            JOIN site_type s ON s.type_id = t.site_type
                            WHERE t.date_add IS NOT NULL
                            GROUP BY s.type_group, date
                        ) t0
                        LEFT JOIN (
                            SELECT s.type_group, DATE_FORMAT(t.date_add, '%Y-%m') date, COUNT(*) total
                            FROM transactions t
                            JOIN site_type s ON s.type_id = t.site_type
                            WHERE t.status_id = 4
                            GROUP BY s.type_group, date
                        ) t1 ON t1.type_group = t0.type_group AND t1.date = t0.date
                        LEFT JOIN (
                            SELECT s.type_group, DATE_FORMAT(t.date_add, '%Y-%m') date, COUNT(*) total
                            FROM transactions t
                            JOIN site_type s ON s.type_id = t.site_type
                            WHERE t.status_id <> 4 AND t.payment_status NOT IN (7210, -21, -22, -23, -24)
                            GROUP BY s.type_group, date
                        ) t2 ON t2.type_group = t0.type_group AND t2.date = t0.date
                        LEFT JOIN (
                            SELECT s.type_group, DATE_FORMAT(t.date_add, '%Y-%m') date, COUNT(*) total
                            FROM transactions t
                            JOIN site_type s ON s.type_id = t.site_type
                            WHERE t.status_id <> 4 AND t.payment_status IN (7210, -21, -22, -23, -24)
                            GROUP BY s.type_group, date
                        ) t3 ON t3.type_group = t0.type_group AND t3.date = t0.date
                        ORDER BY t0.type_group, date DESC";
                break;

            case 3: // group by ATM, Credit
                $sql = "SELECT
                            IF (t.bank_code = '123PCC', 'Credit', 'ATM') payment_group,
                            DATE_FORMAT(t.date_add, '%Y-%m') date,
                            COUNT(*) total
                        FROM transactions t
                        WHERE t.status_id <> 4 AND SUBSTR(t.bank_code, 1, 4) = '123P'
                            AND t.payment_status NOT IN (7210, -21, -22, -23, -24)
                        GROUP BY payment_group, date
                        ORDER BY payment_group, date DESC";
                break;
        }

        $data = isset($sql) ? $this->_db->fetchAll($sql) : false;

        $this->memcache->set($key, $data, 3600);

        return $data;
    }

    public function getReportDataGroupByCineplex() {
        return $this->getReportData(1);
    }

    public function getReportDataGroupByStatus() {
        return $this->getReportData(2);
    }

    public function getReportDataGroupByPayment() {
        return $this->getReportData(3);
    }
    public function getReportDataGroupByStatusPaymentType($payment_type) {
        $key = sprintf('admin.report_by_payment_type.%s', $payment_type);
        $data = $this->memcache->get($key);
        if ($data) return $data;

        $where ='';
        switch ($payment_type) {
            case 0:
                break;
            case 1:
                 $where = "AND SUBSTR(t.bank_code, 1, 4) = '123P'";
                break;
            case 2:
                 $where = "AND (t.payment_type='MOMO' OR t.payment_type ='MOMO_APP' OR t.payment_type ='MSERVICE_APP')";
                break;
            case 3:
                 $where = "AND t.payment_type='PLUS'";
                break;
            case 4:
                 $where = "AND t.payment_type='ZALO'";
                break;
            default:
                # code...
                break;
        }


        $sql = "SELECT
                    t0.type_group, t0.date, t0.total,
                    IFNULL(t1.total, 0) total_success,
                    IFNULL(t2.total, 0) total_fail,
                    IFNULL(t3.total, 0) total_cancel
                FROM (
                    SELECT s.type_group, DATE_FORMAT(t.date_add, '%Y-%m') date, COUNT(*) total
                    FROM transactions t
                    JOIN site_type s ON s.type_id = t.site_type $where
                    WHERE t.date_add IS NOT NULL
                    GROUP BY s.type_group, date
                ) t0
                LEFT JOIN (
                    SELECT s.type_group, DATE_FORMAT(t.date_add, '%Y-%m') date, COUNT(*) total
                    FROM transactions t
                    JOIN site_type s ON s.type_id = t.site_type $where
                    WHERE t.status_id = 4
                    GROUP BY s.type_group, date
                ) t1 ON t1.type_group = t0.type_group AND t1.date = t0.date
                LEFT JOIN (
                    SELECT s.type_group, DATE_FORMAT(t.date_add, '%Y-%m') date, COUNT(*) total
                    FROM transactions t
                    JOIN site_type s ON s.type_id = t.site_type $where
                    WHERE t.status_id <> 4 AND t.payment_status NOT IN (7210, -21, -22, -23, -24)
                    GROUP BY s.type_group, date
                ) t2 ON t2.type_group = t0.type_group AND t2.date = t0.date
                LEFT JOIN (
                    SELECT s.type_group, DATE_FORMAT(t.date_add, '%Y-%m') date, COUNT(*) total
                    FROM transactions t
                    JOIN site_type s ON s.type_id = t.site_type $where
                    WHERE t.status_id <> 4 AND t.payment_status IN (7210, -21, -22, -23, -24)
                    GROUP BY s.type_group, date
                ) t3 ON t3.type_group = t0.type_group AND t3.date = t0.date
                ORDER BY t0.type_group, date DESC";

        $data = isset($sql) ? $this->_db->fetchAll($sql) : false;

        $this->memcache->set($key, $data, 3600);

        return $data;
    }
}