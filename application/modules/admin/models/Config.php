<?php

class Admin_Model_Config extends My_Model_Abstract {

    protected $_name    = 'Config';
    protected $_primary = 'config_id';

    public function getConfig(){
    	$sql = 'SELECT  *
                FROM Config
                LIMIT 1';
       return  $this->_db->fetchRow($sql);
    }

}