<?php

class Admin_Model_Opal extends My_Model_Abstract {

    protected $_name    = 'news';
    protected $_primary = 'news_id';

    public function getList($offset =0,$count=5) {
        $sql = "SELECT SQl_CALC_FOUND_ROWS n.news_id,n.news_title
                ,n.news_description,n.type_id,n.source_id,n.date_add,n.is_active,n.thumbnail
                FROM news n
                ORDER BY n.date_add DESC
                LIMIT $offset, $count
                ";

        $sql2 = "SELECT FOUND_ROWS()";

        $rows =  $this->_db->fetchAll($sql);

        $total = (int)$this->_db->fetchOne($sql2);

        return array('rows' => $rows, 'total' => $total);
    }

}