<?php

class Admin_Model_Cinema extends My_Model_Abstract {

    protected $_name    = 'cinemas';
    protected $_primary = 'cinema_id';


    public function getDetail($id) {


    }

    public function getList() {
        $sql = 'SELECT c.*,l.city_name as cinema_location
                FROM cinemas c
                LEFT JOIN locations l ON l.location_id = c.location_id
                ORDER BY cinema_id DESC';

        return $this->_db->fetchAll($sql);
    }

}