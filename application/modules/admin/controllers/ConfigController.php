<?php

class Admin_ConfigController extends My_Controller_Form {

    public $_form = 'Config';

    public function detailAction() {
        $data =  $this->model->Config->getConfig();
        if ($data && $data->config_id > 0) {
            $this->view->data = $this->getDetail($data->config_id);
        }
        $this->view->form = Admin_Model_Form::get($this->_form);
    }


}

