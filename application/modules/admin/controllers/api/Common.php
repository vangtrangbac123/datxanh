<?php

class Admin_Api_Common extends My_Api_Abstract {

    public function getNewVersion($params) {

        if (!isset($params['version_id'])) {
            throw new Exception('Param version_id not found');
        }

        $version = $this->model->AppVersion->getNewVersion($params['version_id']);
        if (!$version) return 0;
        return $version;
    }
     public function getCinema($params) {

        if (!isset($params['id'])) {
            throw new Exception('Param id not found');
        }

        $cinema = $this->model->Cinema->get($params['id']);
        if (!$cinema) return 0;
        return $cinema;
    }
}
