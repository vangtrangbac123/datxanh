<?php

class Admin_AjaxController extends My_Controller_Action {

    public $api;

    public function init() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        header('Content-type: application/json; charset=utf-8');
        //$this->api = new Api_Backend();
    }

    public function uploadAction(){
        echo json_encode('value');die;
    }


    public function artistAction() {
        $s = $this->_getParam('s');
        $sql = 'SELECT artist_id id, artist_name `text` FROM artists WHERE artist_name LIKE "%'.$s.'%"';
        $rows = $this->model->Artist->getRows($sql);
        echo json_encode($rows);
        die;
    }

    public function checkartistAction() {
        $s = $this->_getParam('s');
        $sql = 'SELECT artist_id id, artist_name `text` FROM artists WHERE artist_name = "'.$s.'" LIMIT 1' ;
        $rows = $this->model->Artist->getRows($sql);

        if(empty($rows)) {echo json_encode(false);die;}
        echo json_encode(true);
        die;
    }

    public function addActorAction() {
        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();

            if (!isset($post['film_id']) || !isset($post['artist_id']) || !isset($post['type_id'])) {
                echo Zend_Json::encode(false);
                die;
            }
            $data = array(
                'film_id' => (int)$post['film_id'],
                'artist_id' => (int)$post['artist_id'],
                'char_name' => trim($post['char_name']) ,
                'is_active' => 1,
                'type_id' => (int)$post['type_id'],
                'number' => (int)$post['number'],
                'date_add' => date('Y-m-d h:i:s')
            );
            $r = $this->model->FilmArtist->insert($data);

            if ($r > 0) {
                $sql = "SELECT
                     a.artist_name ,
                     i.path,
                     fa.char_name
                FROM artists a
                LEFT JOIN artist_image ai ON ai.artist_id = a.artist_id and ai.type_id =1 and ai.is_active =1
                LEFT JOIN images i ON i.image_id = ai.image_id
                LEFT JOIN film_artist fa ON fa.artist_id = a.artist_id
                WHERE fa.film_id =:film_id AND fa.artist_id =:artist_id
                GROUP BY a.artist_id
                ORDER BY a.is_active DESC, a.artist_name";

                $result = $this->model->Artist->getRow($sql, array(
                    'artist_id' => (int)$post['artist_id'],
                    'film_id' => (int)$post['film_id']
                ));
                echo Zend_Json::encode($result);
                die;
            }

            echo Zend_Json::encode(false);
            die;
        }
    }


    public function deleteAction() {
        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();

            $data = array();
            $result = false;

            switch (strtolower($post['type'])) {

                case 'category':
                   $result = $this->model->Category->remove($post['id']);
                   break;

                case 'event':
                    $result = $this->model->Event->remove($post['id']);
                    break;

                case 'banner':
                    $result = $this->model->Banner->remove($post['id']);
                    break;

                case 'news':
                    $result = $this->model->News->remove($post['id']);
                    break;
            }


            echo Zend_Json::encode($result ? true : false);
            die;
        }
    }


    public function activeAction() {
        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();

            $id = $post['id'];
            $field = (isset($post['field']) && !empty($post['field'])) ? trim($post['field']) : 'is_active';
            $table = ucfirst($post['table']);

            if ($table == 'TmpFilm') $field = 'is_voice';
            elseif ($table == 'Bank') $field = 'status';

            $isActive = ((int)$post['is_active'] == 1) ? 1 : 0;
            $result = $this->model->{$table}->updateActive(array(
                $field => $isActive
            ) , $id);
            echo Zend_Json::encode($result);
            die;
        }
    }


    public function imageAction() {
        if ($this->getRequest()->isPost()) {
            $params = $this->getRequest()->getParams();

            if (!isset($params['pkid']) || !isset($params['form']) || !isset($params['url']) || empty($params['url'])) {
                echo Zend_Json::encode(false);
                die;
            }

            $db = Admin_Model_Form::get($params['form']);
            $model = $db['model'];
            $primary = $db['primary'];


            switch ($model) {
                case 'Banner':
                    $this->bannerImage($params);
                case 'Cinema':
                    $this->cinemaImage($params);
                break;
                case 'Trailer':
                    $this->trailerImage($params);
                break;
                case 'Product':
                    $this->ProductImage($params);
                break;
                case 'News':
                    $this->newsImage($params);
                break;
            }
        }
    }

    public function resizejobAction(){
         $this->model->ImageFilm->resizeJob();
    }


    public function galleryAction() {

        if ($this->getRequest()->isPost()) {
            $params = $this->getRequest()->getParams();
            if (!isset($params['type_id']) || !isset($params['urls'])) {
                echo json_encode('false');
                die;
            }

            $typeId = $params['type_id'];
            $urlArr = $params['urls'];


            foreach ($urlArr as $key => $url) {
                $h3 = 100;
                $h4 = 200;
                /* $data = getimagesize($url);
                $width = $data['0'];
                $height = $data['1'];*/
                //echo Zend_Json::encode($width);die;
                if (empty($url)) {
                    continue;
                }

                // origin image
                $size1 = $url;
                //Insert Image
                $imageId = $this->model->Image->insertIgnore(array(
                    'image_id' => '',
                    'type' => $typeId,
                    'status_id' => 1,
                    'date_add' => date('y-m-d h:i:s') ,
                    'is_resize' => 0,
                    'path'  => Utility_Unicode::getPath($url),
                    'size1' => Utility_Unicode::getPath($size1)
                ));
            }

            echo json_encode('true');
            die;
        }
    }

    public function gallerycameraAction() {

        if ($this->getRequest()->isPost()) {
            $params = $this->getRequest()->getParams();
            if (!isset($params['gallery_id']) || !isset($params['urls']) || !isset($params['type_id'])) {
                echo json_encode('false');
                die;
            }

            $galleryId = $params['gallery_id'];
            $typeId = $params['type_id'];
            $urlArr = $params['urls'];



            $sql = 'SELECT image_id FROM gallery_image WHERE is_active = 1 AND type_id =:type_id AND gallery_id =:gallery_id';
            $imageArr = $this->model->ImageGallery->getRows($sql, array(
                'gallery_id' => $galleryId,
                'type_id' => $typeId
            ));



             $del = array();
             $del['table']   = 'gallery_image';
             $del['gallery_id'] = $galleryId;
             $del['type_id'] = $typeId;


             Api_Mapi::delete($del);

             $del = array();
            foreach ($imageArr as $key => $img) {
                   $del['image_id'] = $img->image_id;
                   $del['table']    = 'images';
                   Api_Mapi::delete($del);
            }


            foreach ($urlArr as $key => $url) {

                if (empty($url)) {
                    continue;
                }


                $size1 = Utility_Image::resize($url, 100, 100);
                $size2 = Utility_Image::resize($url, 600, 600);
                $size3 = Utility_Image::resize($url, 1200, 1200);

                //Insert Image
                $imageId = $this->model->Image->insertIgnore(array(
                    'image_id'  => '',
                    'type'      => $typeId,
                    'status_id' => 1,
                    'date_add'  => date('y-m-d h:i:s') ,
                    'is_resize' => 0,
                    'path'      => $url,
                    'size1'     => $size1,
                    'size2'     => $size2,
                    'size3'     => $size3,
                ));
                //Insert Film image
                $this->model->ImageGallery->insertIgnore(array(
                    'image_id'   => $imageId,
                    'gallery_id' => $galleryId,
                    'is_active'  => 1,
                    'type_id'    => $typeId,
                    'date_add'   => date('y-m-d h:i:s')
                ));
            }

            echo json_encode('true');
            die;
        }
    }

    public function galleryRemoveAction() {

        if ($this->getRequest()->isPost()) {
            $params = $this->getRequest()->getParams();
            if (!isset($params['image_id'])) {
                echo json_encode('false');
                die;
            }

            $this->model->Image->delImage($params['image_id']);


            echo json_encode('true');
            die;
        }
    }

     public function galleryCameraRemoveAction() {
        if ($this->getRequest()->isPost()) {
            $params = $this->getRequest()->getParams();
            if (!isset($params['gallery_id']) || !isset($params['image_id'])) {
                echo json_encode('false');
                die;
            }

            $galleryId = $params['gallery_id'];
            $typeId = $params['type_id'];
            $imageId = $params['image_id'];

             $del = array();
             $del['table']    = 'film_image';
             $del['gallery_id']  = $galleryId;
             $del['image_id'] = $imageId;

             if (count($del) > 0) {
                   Api_Mapi::delete($del);
                   unset($del['gallery_id']);
                   $del['table'] = 'images';
                   Api_Mapi::delete($del);
              }


            echo json_encode('true');
            die;
        }
    }

    public function bannerImage($params) {

        $bannerId = $params['pkid'];
        $size = $params['size'];
        $path = $params['url'];
        $typeId = $params['type'];
        //Check image Old
        $data = array(
            'banner_id'  => $bannerId,
            'banner_image' => $path
        );


        $bannerId = $this->model->Banner->save($data);

        if ($bannerId) echo Zend_Json::encode($bannerId);
    }

    public function cinemaImage($params) {

        $cinemaId = $params['pkid'];
        $size = $params['size'];
        $path = $params['url'];
        $typeId = $params['type'];

        $data = array();
        $data['cinema_id'] = $cinemaId;

        if($size == 1) $data['cinema_image'] = $path;
        if($size == 2) $data['cinema_image_large'] = $path;
        if($size == 3) $data['list_price'] = $path;

        $imageId = $this->model->Cinema->save($data);

        if ($imageId) echo Zend_Json::encode($imageId);
    }

    public function trailerImage($params) {

        $cinemaId = $params['pkid'];
        $size = $params['size'];
        $path = $params['url'];
        $typeId = $params['type'];

        $data = array(
            'trailer_id' => $cinemaId,
            'thumbnail'  => $path
        );

        $imageIds = $this->model->Trailer->save($data);

        if ($imageId) echo Zend_Json::encode($imageId);
    }




    public function newsImage($params) {

        $newsId = $params['pkid'];
        $size = $params['size'];
        $path = $params['url'];
        $typeId = $params['type'];
        //Check image Old
        $dataCheck = array(
            'type_id' => $typeId,
            'news_id' => $newsId
        );

        $imgOld = $this->model->ImageNews->check($dataCheck);
        //Insert or Update
        $dataImage = array(
            'image_id' => '',
            'type' => $typeId,
            'status_id' => 1,
            'date_add' => date('y-m-d h:i:s') ,
            'is_resize' => 1
        );

        $tmp = array();
        $tmp['1'] = 3;
        $tmp['4'] = 4;
        $tmp['5'] = 1;
        $tmp['6'] = 1;
        $tmp['7'] = 1;

        if (isset($tmp[$typeId]) && $tmp[$typeId] == $size) {
            $dataImage['path'] = $path;
            switch ($typeId) {
                case 1:
                   $w2 = 300; $h2 = 150;
                   $w1 = 100;  $h1 = 100;
                    break;

                case 4:
                   $w3 = 130; $h3 = 130;
                   $w2 = 100; $h2 = 100;
                   $w1 = 65;  $h1 = 65;
                    break;
            }
        }

        if ($size == 1) $dataImage['size1'] = $path;
        if ($size == 2) $dataImage['size2'] = $path;
        if ($size == 3) $dataImage['size3'] = $path;
        if ($size == 4) $dataImage['size4'] = $path;
        if ($size == 5) $dataImage['size5'] = $path;
        if ($size == 6) $dataImage['size6'] = $path;

        if ($imgOld > 0) {
            $dataImage['image_id'] = $imgOld;
            $imageId = $this->model->Image->save($dataImage);
        } else {

            if(isset($w1))  $dataImage['size1']  = Utility_Image::resizeMyImage($path , $w1, $h1);
            if(isset($w2))  $dataImage['size2']  = Utility_Image::resizeMyImage($path , $w2, $h2);
            if(isset($w3))  $dataImage['size3']  = Utility_Image::resizeMyImage($path , $w3, $h3);


            $imageId = $this->model->Image->save($dataImage);
            if ($imageId > 0) {
                $data = array(
                    'image_id' => $imageId,
                    'news_id' => $newsId,
                    'is_active' => 1,
                    'type_id' => $typeId,
                    'date_add' => date('y-m-d h:i:s')
                );
                $this->model->ImageNews->insertIgnore($data);
            }
        }
        if ($imageId) echo Zend_Json::encode($imageId);
    }


    public function productImage($params) {

        $pId = $params['pkid'];
        $size = $params['size'];
        $path = $params['url'];
        $typeId = $params['type'];
        //Check image Old
        $dataCheck = array(
            'type_id' => $typeId,
            'product_id' => $pId
        );

        $imgOld = $this->model->ImageProduct->check($dataCheck);
        //Insert or Update
        $dataImage = array(
            'image_id' => '',
            'type' => $typeId,
            'status_id' => 1,
            'date_add' => date('y-m-d h:i:s') ,
            'is_resize' => 1
        );

        $tmp = array();
        $tmp['1'] = '2';
        $tmp['4'] = '4';
        $tmp['5'] = '3';
        $tmp['6'] = '1';
        $tmp['8'] = '1';

        if (isset($tmp[$typeId]) && $tmp[$typeId] == $size) {
            $dataImage['path'] = $path;
            switch ($typeId) {
                case 1:
                   $w1 = 300;  $h1 = 150;
                    break;

                case 4:
                   $w3 = 130; $h3 = 130;
                   $w2 = 100; $h2 = 100;
                   $w1 = 65;  $h1 = 65;

                    break;
                case 5:
                   $w2 = 220; $h2 = 300;
                   $w1 = 160; $h1 = 230;

                    break;
            }
        }

        if ($size == 1) $dataImage['size1'] = $path;
        if ($size == 2) $dataImage['size2'] = $path;
        if ($size == 3) $dataImage['size3'] = $path;
        if ($size == 4) $dataImage['size4'] = $path;
        if ($size == 5) $dataImage['size5'] = $path;
        if ($size == 6) $dataImage['size6'] = $path;

        if ($imgOld > 0) {
            $dataImage['image_id'] = $imgOld;
            $imageId = $this->model->Image->save($dataImage);
        } else {

            if(isset($w1))  $dataImage['size1']  = Utility_Image::resizeMyImage($path , $w1, $h1);
            if(isset($w2))  $dataImage['size2']  = Utility_Image::resizeMyImage($path , $w2, $h2);
            if(isset($w3))  $dataImage['size3']  = Utility_Image::resizeMyImage($path , $w3, $h3);


            $imageId = $this->model->Image->save($dataImage);
            if ($imageId > 0) {
                $data = array(
                    'image_id' => $imageId,
                    'product_id' => $pId,
                    'is_active' => 1,
                    'type_id' => $typeId,
                    'date_add' => date('y-m-d h:i:s')
                );
                $this->model->ImageProduct->insertIgnore($data);
            }
        }
        if ($imageId) echo Zend_Json::encode($imageId);
    }

    public function sessionAction() {
        print_r($_SESSION);
    }

    public function pictureAction() {
        $id = $this->_getParam('id');
        $type = $this->_getParam('type');
        $default = '/layouts/v2/img/avatar.png';
        $url = null;
        switch ($type) {
            case 'news':
                $sql = 'SELECT i.size1 FROM news_image ni
                        JOIN images i ON i.image_id = ni.image_id AND ni.type_id = 4
                        WHERE ni.news_id = :news_id
                        ORDER BY i.date_add DESC
                        LIMIT 1';
                $url = $this->model->News->getOne($sql, array('news_id' => $id));
                break;
        }
        $this->_redirect(empty($url) ? $default : $url);
    }

    public function dashboardAction() {
        $result = false;
        $type = $this->_getParam('type');
        switch ($type) {
            case 'device':
                $result = $this->_dashboardDevice();
                break;

            case 'transaction';
                $result = $this->_dashboardTransaction();
                break;

            case 'report_trans_cineplex':
                $result = $this->model->Dashboard->getReportDataGroupByCineplex();
                break;

            case 'report_trans_status':
                $result = $this->model->Dashboard->getReportDataGroupByStatus();
                break;

            case 'report_trans_payment':
                $result = $this->model->Dashboard->getReportDataGroupByPayment();
                break;
            case 'report_trans_status_by_payment_type':
                $payment_type =  $this->_getParam('payment_type');
                $result = $this->model->Dashboard->getReportDataGroupByStatusPaymentType($payment_type);
                break;
        }

        echo json_encode($result);
    }

    private function _dashboardDevice() {
        $group = array();
        $group['1']['label'] = 'iOS';
        $group['1']['color'] = '#734ba9';
        $group['1']['data']  = array();

        $group['2']['label'] = 'Android';
        $group['2']['color'] = '#2baab1';
        $group['2']['data']  = array();

        $group['3']['label'] = 'Windows Phone';
        $group['3']['color'] = '#cccccc';
        $group['3']['data']  = array();

        $rows = $this->model->Dashboard->getListInstalledByMonth();
        foreach ($rows as $row) {
            $group[$row->type_id]['data'][] = array($row->label, (int)$row->total);
        }

        return array_values($group);
    }

    private function _dashboardTransaction() {
        $group = array();
        $group['1']['label'] = 'Transaction';
        $group['1']['color'] = '#734ba9';
        $group['1']['data']  = array();

        $group['2']['label'] = 'Total Revenue (million)';
        $group['2']['color'] = '#2baab1';
        $group['2']['data']  = array();

        $rows = $this->model->Dashboard->getListTransaction();
        foreach ($rows as $row) {
            $group['1']['data'][] = array($row->label, (int)$row->total);
            $group['2']['data'][] = array($row->label, (int)$row->total_revenue / 1000000);
        }

        return array_values($group);
    }
}
