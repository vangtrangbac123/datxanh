<?php

class Admin_OpalController extends My_Controller_Form {

    public $_form = 'Opal';
    private $api;

    public function getList() {
        $is_active = (int)$this->_getParam('is_active', -1);

        $bin =array();
        $where ='';

        if($is_active != -1 ){
            $where .=' AND n.is_active = :is_active';
            $bin['is_active'] = $is_active;
        }


        $sql = "SELECT SQL_CALC_FOUND_ROWS
                    s.*, n.*
                FROM news n
                LEFT JOIN seo s ON s.news_id = n.news_id
                WHERE n.type_id = 1 $where
                ORDER BY  n.date_add DESC";
        $data = $this->getListAutoPaging($sql,$bin);

        $data['sort'] = new stdClass();
        if($is_active != -1 ) $data['sort']->is_active = $is_active;
        return $data;
    }


    public function onSaveBefore($data, $post) {


        $title  = trim($data['news_title']);
        $newsId = isset($data['news_id']) ? $data['news_id'] : null;

        $data['news_title_escape'] = $this->view->escape($title);
        $data['news_title_ascii']  = Utility_Unicode::get_utf8_to_ascii($title);
        $data['news_slug']         = Utility_Unicode::get_str_replace($title);
        $data['url']               = $data['news_slug'];
        $data['type_id']           = 1;

        if ($newsId) {
           $data['news_url_short'] = '/opalriverside/'.$data['news_slug'];
        }

        if (isset($data['content'])) {
            $data['content_master'] = $data['content'];
            $data['content'] = Utility_Shortcode::convert($data['content']);
        }

        return $data;

    }

    public function onSaveAfter($id, $data) {
        $meta = array();
        $meta['meta_title']       = $data['meta_title'];
        $meta['meta_keyword']     = $data['meta_keyword'];
        $meta['meta_description'] = $data['meta_description'];
        $meta['og_title']         = $data['og_title'];
        $meta['og_description']   = $data['og_description'];
        $meta['og_url']           = $data['og_url'];
        $meta['og_image']         = $data['og_image'];
        $meta['news_id']          = $id;
        $meta['page']             = $data['news_url_short'];

        $this->updatemeta($id, $meta);
        $this->updateTags($id, $data['news_tags']);
        return $data;
    }

    private function updateTags($newsId, $tags) {
        $newsId = intval($newsId);
        if ($newsId == 0) return;

        $this->model->Tag->delNewsTag($newsId);

        $tags = trim($tags);
        if ($tags == '') return;

        $tags = explode(',', $tags);
        if (count($tags) == 0) return;

        $sep = '';
        $sql = 'INSERT IGNORE INTO news_tag (`news_id`, `tag_id`) VALUES ';
        foreach ($tags as $name) {
            $row = $this->model->Tag->checkTag(trim($name));
            if ($row) {
                $tagId = (int)$row->tag_id;
                $sql .= $sep . "($newsId, $tagId)";
                $sep = ', ';
            }
        }

        return $this->model->Tag->_excute($sql);
    }

    private function updatemeta($newsId, $meta) {
        $newsId = intval($newsId);
        if ($newsId == 0) return;
        $meta['meta_id'] = null;

        $r = $this->model->Seo->delMeta($newsId);
        return $this->model->Seo->save($meta);
    }
}

