<?php

class Admin_NewsController extends My_Controller_Form {

    public $_form = 'News';
    private $api;

    public function getList() {
        $is_active = (int)$this->_getParam('is_active', -1);

        $bin =array();
        $where ='';

        if($is_active != -1 ){
            $where .=' AND n.is_active = :is_active';
            $bin['is_active'] = $is_active;
        }


        $sql = "SELECT SQL_CALC_FOUND_ROWS
                    s.*, n.*, i.size2
                FROM news n
                LEFT JOIN news_image ni ON ni.news_id = n.news_id AND ni.type_id = 1 AND ni.is_active = 1
                LEFT JOIN images i ON i.image_id = ni.image_id
                LEFT JOIN seo s ON s.news_id = n.news_id
                WHERE n.type_id = 2  $where
                ORDER BY  n.date_add DESC";
        $data = $this->getListAutoPaging($sql,$bin);

        $data['sort'] = new stdClass();
        if($is_active != -1 ) $data['sort']->is_active = $is_active;
        return $data;
    }

    private function queryImage($data) {
        if (!isset($data['rows'])) return $data;
        foreach ($data['rows'] as $row) {
            $sql = 'SELECT i.size1 FROM news_image ni
                    JOIN images i ON i.image_id = ni.image_id AND ni.type_id = 4
                    WHERE ni.news_id = :news_id
                    ORDER BY i.date_add DESC
                    LIMIT 1';
            $row->size1 = $this->model->News->getOne($sql, array('news_id' => $row->news_id));
        }
        return $data;
    }

    public function getDetail($newsId){
       $news = $this->model->News->get($newsId);
        $sql = 'SELECT ni.type_id, i.*
                FROM news_image ni
                JOIN images i ON i.image_id = ni.image_id
                WHERE ni.news_id = :news_id
                ORDER BY ni.type_id';
        $images = $this->model->News->getRows($sql, array('news_id' => $newsId));

        if(!empty($news->list_product_id)){
            $list_product_id = explode(',',$news->list_product_id);
            $arr = array();
            foreach ($list_product_id as $key => $product_id) {
                array_push($arr,(object)array('id' => $product_id));
            }
            $news->list_product_id = $arr;
        }else{
            $news->list_product_id =array();
        }
        
        $group = array();
        foreach ($images as $img) {
            $group[$img->type_id][] = $img;
            $news->{sprintf('image_%d_%d', $img->type_id, 1)} = $img->size1;
            $news->{sprintf('image_%d_%d', $img->type_id, 2)} = $img->size2;
            $news->{sprintf('image_%d_%d', $img->type_id, 3)} = $img->size3;
            $news->{sprintf('image_%d_%d', $img->type_id, 4)} = $img->size4;
            $news->{sprintf('image_%d_%d', $img->type_id, 5)} = $img->size5;
            $news->{sprintf('image_%d_%d', $img->type_id, 6)} = $img->size6;
        }
        $news->images = $group;
        return $news;
    }

    public function detailAction(){
        $id = (int)$this->_getParam('id', 0);
        if ($id > 0) {
            $this->view->data = $this->getDetail($id);
        }
        $this->view->form = Admin_Model_Form::get($this->_form);
    }



    public function onSaveBefore($data, $post) {


        $title  = trim($data['news_title']);
        $newsId = isset($data['news_id']) ? $data['news_id'] : null;

        $data['news_title_escape'] = $this->view->escape($title);
        $data['news_title_ascii']  = Utility_Unicode::get_utf8_to_ascii($title);
        $data['news_slug']         = Utility_Unicode::get_str_replace($title);
        $data['url']               = $data['news_slug'];
        $data['type_id']           = 2;


        if (isset($data['content'])) {
            $data['content_master'] = $data['content'];
            $data['content'] = Utility_Shortcode::convert($data['content']);
        }

        $data['news_url_short'] =  '/tin-tuc/'.$data['news_slug'];
        return $data;

    }

    public function onSaveAfter($id, $data) {
        $meta = array();
        $meta['meta_title']       = $data['meta_title'];
        $meta['meta_keyword']     = $data['meta_keyword'];
        $meta['meta_description'] = $data['meta_description'];
        $meta['og_title']         = $data['og_title'];
        $meta['og_description']   = $data['og_description'];
        $meta['og_url']           = $data['og_url'];
        $meta['og_image']         = $data['og_image'];
        $meta['news_id']          = $id;
        $meta['page']             = $data['news_url_short'];

        $this->updatemeta($id, $meta);
        $this->updateTags($id, $data['news_tags']);
        return $data;
    }

    private function updateTags($newsId, $tags) {
        $newsId = intval($newsId);
        if ($newsId == 0) return;

        $this->model->Tag->delNewsTag($newsId);

        $tags = trim($tags);
        if ($tags == '') return;

        $tags = explode(',', $tags);
        if (count($tags) == 0) return;

        $sep = '';
        $sql = 'INSERT IGNORE INTO news_tag (`news_id`, `tag_id`) VALUES ';
        foreach ($tags as $name) {
            $row = $this->model->Tag->checkTag(trim($name));
            if ($row) {
                $tagId = (int)$row->tag_id;
                $sql .= $sep . "($newsId, $tagId)";
                $sep = ', ';
            }
        }

        return $this->model->Tag->_excute($sql);
    }

    private function updatemeta($newsId, $meta) {
        $newsId = intval($newsId);
        if ($newsId == 0) return;
        $meta['meta_id'] = null;

        $r = $this->model->Seo->delMeta($newsId);
        return $this->model->Seo->save($meta);
    }
}

