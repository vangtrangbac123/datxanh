<?php

class Admin_CategoryController extends My_Controller_Form {

    public $_form = 'Category';

     public function onSaveAfter($id, $data) {
        if (!empty($data['category_name'])) {
        	$data['category_slug'] = Utility_Unicode::get_str_replace($data['category_name']);
            $data['url_short'] = '/'.$data['category_slug'];
        }
        $data['category_id'] = $id;
        return  $this->model->Category->save($data);
    }

}

