            <!-- start: sidebar -->
            <aside id="sidebar-left" class="sidebar-left">

                <div class="sidebar-header">
                     <div class="sidebar-title">
                        <?php echo $this->user->fullname?> | <a href="/auth/logout/" title="Sign Out">Sign Out</a>
                    </div>
                    <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                        <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                    </div>
                </div>

                <div class="nano">
                    <div class="nano-content">
                        <nav  class="nav-main" role="navigation">
                            <ul class="nav nav-main">
                                <li class="nav-parent" data-controller="news">
                                    <a>
                                        <i class="fa fa-copy" aria-hidden="true"></i>
                                        <span>News</span>
                                    </a>
                                    <ul class="nav nav-children">
                                        <li data-action="detail">
                                            <a href="/admin/news/detail/">Add News</a>
                                        </li>
                                        <li data-action="index">
                                            <a href="/admin/news/">List Post</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>

                        <nav  class="nav-main" role="navigation">
                            <ul class="nav nav-main">
                                <li class="nav-parent" data-controller="opal">
                                    <a>
                                        <i class="fa fa-copy" aria-hidden="true"></i>
                                        <span>Opalriversides</span>
                                    </a>
                                    <ul class="nav nav-children">
                                        <li data-action="detail">
                                            <a href="/admin/opal/detail/">Add News</a>
                                        </li>
                                        <li data-action="index">
                                            <a href="/admin/opal/">List Post</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>

                        <nav  class="nav-main" role="navigation">
                            <ul class="nav nav-main">
                                <li class="nav-parent" data-controller="opal">
                                    <a>
                                        <i class="fa fa-copy" aria-hidden="true"></i>
                                        <span>SEO</span>
                                    </a>
                                    <ul class="nav nav-children">
                                        <li data-action="detail">
                                            <a href="/admin/seo/detail/">Add Page</a>
                                        </li>
                                        <li data-action="index">
                                            <a href="/admin/seo/">List Page</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>

                        <nav  class="nav-main" role="navigation">
                            <ul class="nav nav-main">
                                <li class="nav-parent" data-controller="gallery">
                                    <a>
                                        <i class="fa fa-copy" aria-hidden="true"></i>
                                        <span>Gallery</span>
                                    </a>
                                    <ul class="nav nav-children">
                                        <li data-action="detail">
                                            <a href="/admin/gallery/detail/">Gallery</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>

                        <nav id="menu" class="nav-main" role="navigation">
                            <ul class="nav nav-main">
                                <li class="nav-parent" data-controller="config">
                                    <a>
                                        <i class="fa fa-copy" aria-hidden="true"></i>
                                        <span>Config site</span>
                                    </a>
                                    <ul class="nav nav-children">
                                        <li data-action="detail">
                                            <a href="/admin/config/detail/">Config</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>

                        <nav id="menu" class="nav-main" role="navigation">
                            <ul class="nav nav-main">
                                <li class="nav-parent" data-controller="">
                                    <a href="/admin/go123" target="_blank" >
                                        <i class="fa fa-copy" aria-hidden="true"></i>
                                        <span>Tool Upload</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>

                        <hr class="separator">

                        <hr class="separator">

                        <div class="sidebar-widget widget-tasks">
                            <div class="widget-header">
                                <h6>Others</h6>
                            </div>
                            <div class="widget-content">
                                <ul class="list-unstyled m-none">
                                    <li><a href="#">Clear cache</a></li>
                                    <li><a href="#">Old version</a></li>
                                </ul>
                            </div>
                        </div>

                    </div>

                </div>

            </aside>
            <!-- end: sidebar -->