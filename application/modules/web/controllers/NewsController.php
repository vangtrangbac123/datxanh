<?php

class Web_NewsController extends My_Controller_Web {

    public function init() {
    }

    public function indexAction() {
    	$this->view->menu_slug = 'tin-tuc';
       	$this->view->list_news = $this->model->News->getListNews();
    }
    public function detailAction(){
    	$params = $this->getRequest()->getParams();
        $news = $this->model->News->getNewsDetail($params['slug']);

        if(!$news && count($news) == 0){
        	var_dump('Not found');
        }
        $list_related = $this->model->News->getListRelate($news->news_id);


        $this->view->news = $news;
        $this->view->list_related = $list_related;
        $this->view->menu_slug = 'tin-tuc';
    }


}
