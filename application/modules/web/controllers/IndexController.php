<?php

class Web_IndexController extends My_Controller_Web {

    public function init() {
    }

    public function indexAction() {
        $news = $this->model->News->getDetail('thong-tin-chung');

        $this->view->news = $news;
        $this->view->menu_slug = 'thong-tin-chung';
       	$this->_helper->viewRenderer('index');
    }
    public function opalriversideAction(){
    	$params = $this->getRequest()->getParams();
        if(!isset($params['catName'])){
            $this->_forward('index');
        }

        if($params['catName'] == 'hinh-anh'){
            $this->_forward('gallery');
        }

        $news = $this->model->News->getDetail($params['catName']);


        $this->view->news = $news;
        $this->view->menu_slug = $params['catName'];
       	$this->_helper->viewRenderer('index');
    }

    public function galleryAction(){
         $sql = 'SELECT  image_id, size1
                FROM images
                WHERE status_id = 1 AND type = 2';
        $this->view->images =  $this->model->News->getRows($sql);
    }


}
