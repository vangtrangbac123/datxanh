<?php

class Web_Model_News extends My_Model_Abstract {

    protected $_name    = 'news';
    protected $_primary = 'news_id';

    public function getListFutured() {
        $sql = "SELECT
                      news_title,
                      news_url_short
                FROM news n
                WHERE  is_active = 1 AND type_id = 2 AND is_hot =1
                ORDER BY number ASC, date_add DESC
                ";

        return  $this->_db->fetchAll($sql);
    }

    public function getListNews() {
        $sql = "SELECT
                      n.news_title,
                      n.news_url_short,
                      n.news_description,
                      i.size2
                FROM news n
                JOIN news_image ni ON ni.news_id = n.news_id AND ni.type_id = 1
                JOIN images i ON i.image_id = ni.image_id
                WHERE  n.is_active = 1 AND n.type_id = 2
                ORDER BY n.number ASC, n.date_add DESC
                ";

        return  $this->_db->fetchAll($sql);
    }

    public function getListRelate($news_id){
        $sql = "SELECT
                      n.news_title,
                      n.news_url_short,
                      n.news_description,
                      i.size2
                FROM news n
                JOIN news_image ni ON ni.news_id = n.news_id AND ni.type_id = 1
                JOIN images i ON i.image_id = ni.image_id
                WHERE  n.is_active = 1 AND n.type_id = 2 AND n.news_id <> :news_id
                ORDER BY n.number ASC, n.date_add DESC
                ";

        return $this->_db->fetchAll($sql, array('news_id' => $news_id));
    }

     public function getDetail($menu_slug) {
        $sql = "SELECT
                      n.news_title,
                      n.news_url_short,
                      n.news_description,
                      n.content
                FROM news n
                WHERE  n.menu_slug  = :menu_slug
                ";

        return  $this->_db->fetchRow($sql, array('menu_slug' => $menu_slug));
    }

    public function getNewsDetail($news_slug) {
        $sql = "SELECT
                      n.news_id,
                      n.news_title,
                      n.news_url_short,
                      n.news_description,
                      n.date_add,
                      n.content
                FROM news n
                WHERE  n.news_slug  = :news_slug
                ";

        return  $this->_db->fetchRow($sql, array('news_slug' => $news_slug));
    }


}