<header>
    <div class="header-top-bar">
        <div  class="container">
            <div class="row"> 
                <img src="/layouts/web/img/phone.png" width="20px" />Hotline: 
                <span style="color:red;font-weight:bold;font-size:18px;">0905 81 82 88</span>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row logo">
            <div class="col-md-4 col-sm-12 hidden-sm  hidden-xs">
                <img src="/layouts/web/img/logo.png">
            </div>
            <div class="col-md-8 hidden-sm  hidden-xs">
                <img src="/layouts/web/img/banner_h.png" height="120px">
            </div>
        </div>
    </div>
    <div class="header-menu  visible-lg visible-md">
        <div class="container">
            <div class="row menu">
                <ul>
                    <li class="<?php echo ($this->menu_slug == 'thong-tin-chung')?'active':''?>"><a href="/opalriverside/thong-tin-chung">Thông tin chung</a></li>
                    <li class="<?php echo ($this->menu_slug == 'vi-tri')?'active':''?>"><a href="/opalriverside/vi-tri">Vị trí</a></li>
                    <li class="<?php echo ($this->menu_slug == 'tien-ich')?'active':''?>"><a href="/opalriverside/tien-ich">Tiện ích</a></li>
                    <li class="<?php echo ($this->menu_slug == 'thiet-ke')?'active':''?>"><a href="/opalriverside/thiet-ke">Thiết kế</a></li>
                    <li class="<?php echo ($this->menu_slug == 'hinh-anh')?'active':''?>"><a href="/opalriverside/hinh-anh">Hình ảnh</a></li>
                    <li class="<?php echo ($this->menu_slug == 'tien-do')?'active':''?>"><a href="/opalriverside/tien-do">Tiến độ</a></li>
                    <li class="<?php echo ($this->menu_slug == 'thanh-toan')?'active':''?>"><a href="/opalriverside/thanh-toan">Thanh toán</a></li>
                    <li class="<?php echo ($this->menu_slug == 'tin-tuc')?'active':''?>"><a href="/tin-tuc">Tin tức</a></li>
                    <li class="<?php echo ($this->menu_slug == 'lien-he')?'active':''?>"><a href="/opalriverside/lien-he">Liên hệ</a></li>
                </ul>
            </div>
        </div>
    </div>

    <!-- -->
        <div class="visible-sm  visible-xs">
            <div class=" navbar navbar-default navbar-fixed-top" role="navigation" id="slide-nav">
                <div class="container">
                    <div> 
                        <img src="/layouts/web/img/phone.png" width="20px" />Hotline: 
                        <span style="color:red;font-weight:bold;font-size:18px;">0905 81 82 88</span>
                    </div>
                     <div class="navbar-header">
                      <a class="navbar-toggle"> 
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                       </a>
                      <a class="navbar-brand" href="#"><img src="/layouts/web/img/logo.png" width="100px" style="margin-top:-10px"></a>
                     </div>
                     <div id="slidemenu">
                      <ul class="nav navbar-nav">
                            <li class="<?php echo ($this->menu_slug == 'thong-tin-chung')?'active':''?>"><a href="/opalriverside/thong-tin-chung">Thông tin chung</a></li>
                            <li class="<?php echo ($this->menu_slug == 'vi-tri')?'active':''?>"><a href="/opalriverside/vi-tri">Vị trí</a></li>
                            <li class="<?php echo ($this->menu_slug == 'tien-ich')?'active':''?>"><a href="/opalriverside/tien-ich">Tiện ích</a></li>
                            <li class="<?php echo ($this->menu_slug == 'thiet-ke')?'active':''?>"><a href="/opalriverside/thiet-ke">Thiết kế</a></li>
                            <li class="<?php echo ($this->menu_slug == 'hinh-anh')?'active':''?>"><a href="/opalriverside/hinh-anh">Hình ảnh</a></li>
                            <li class="<?php echo ($this->menu_slug == 'tien-do')?'active':''?>"><a href="/opalriverside/tien-do">Tiến độ</a></li>
                            <li class="<?php echo ($this->menu_slug == 'thanh-toan')?'active':''?>"><a href="/opalriverside/thanh-toan">Thanh toán</a></li>
                            <li class="<?php echo ($this->menu_slug == 'tin-tuc')?'active':''?>"><a href="/tin-tuc">Tin tức</a></li>
                            <li class="<?php echo ($this->menu_slug == 'lien-he')?'active':''?>"><a href="/lien-he">Liên hệ</a></li>
                      </ul>
                     </div>
                </div>
            </div>
        </div>
</header>