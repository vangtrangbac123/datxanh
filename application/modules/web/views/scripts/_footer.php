<div style="background-color:#015f95">
    <div class="container">
        <div class="row">
            <div class="menu-bottom">
                <ul>
                    <li><a href="/gioi-thieu.htm" rel="nofollow">Giới thiệu</a></li>
                    <li><a href="/huong-dan.htm" rel="nofollow">Hướng dẫn sử dụng</a></li>
                    <li><a href="/quy-dinh.htm" rel="nofollow">Quy định</a></li>
                    <li><a href="/lien-he.htm" rel="nofollow">Liên hệ</a></li>
                    <li><a href="/dieu-khoan.htm" rel="nofollow">Điều khoản thỏa thuận</a></li>
                </ul>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h4> Liên hệ </h4>
                <p class="info_ct">
                    <span>Hot line: <b>0905 81 82 88</b></span>
                    | <a rel="nofollow">Email: <b>info@datxanh.com</b></a>
                </p>
                <p class="info_ct">
                    <span>27 Đinh Bộ Lĩnh, P.24, Quận Bình Thạnh</span>
                </p>
            </div>
            <div class="col-md-4" >
                <h4> Bản đồ </h4>
                <iframe  style="border:2px solid #fff" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3919.0752475384143!2d106.7094972!3d10.8055488!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317528bc9b4e661d%3A0x2a51944dc419dfe0!2zMjcgxJBpbmggQuG7mSBMxKluaCwgUGjGsOG7nW5nIDI0LCBCw6xuaCBUaOG6oW5oLCBI4buTIENow60gTWluaCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1427040507451" frameborder="0" style="border:0"></iframe>
            </div>
        </div>
    </div>
</footer>

