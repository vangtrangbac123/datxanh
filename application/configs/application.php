<?php

$config = array(
    'resources' => array(
        'layout' => array(
            // 'pluginClass' => 'Plugin_Layout'
        ),
        'modules' => array(),
        'frontController' => array(
            'baseUrl' => '',
            'defaultModule' => 'web',
            'moduleDirectory' => APPLICATION_PATH . '/modules',
            'prefixDefaultModule' => true,
            'throwExceptions' => false,
            'params' => array(
                'displayExceptions' => true,
            ),
            'plugins' => array(
                'Route' => 'Plugin_Route'
            ),
        ),
        'db' => array(
            'adapter' => 'Pdo_Mysql',
            'params' => array(
                'charset'  => 'utf8',
                'host'     => '127.0.0.1',
                'dbname'   => 'bds_datxanh',
                'username' => 'root',
                'password' => '',
                'profiler' => true,
                'options' => array(
                    'fetchMode' => Zend_Db::FETCH_OBJ
                ),
            ),
        ),
    ),
);

require APPLICATION_ENV . '/application.php';

if (App::isDebug()) {
    $config['resources']['frontController']['params']['displayExceptions'] = true;
    $config['resources']['db']['params']['profiler'] = true;
}

return $config;