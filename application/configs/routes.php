<?php

return array(

    'indexindex' => new Zend_Controller_Router_Route_Static(
        '',
        array(
            'module'     => 'web',
            'controller' => 'index',
            'action'     => 'index',
        ), array(
        ),
        ''
    ),
    'indexopalriverside' => new Zend_Controller_Router_Route_Regex(
        'opalriverside/([a-zA-Z0-9-]+)',
        array(
            'module'     => 'web',
            'controller' => 'index',
            'action'     => 'opalriverside',
        ), array(
            '1' => 'catName',
        ),
        'opalriverside/%s'
    ),

    'newsdetail' => new Zend_Controller_Router_Route_Regex(
        'tin-tuc/(.*)',
        array(
            'module'     => 'web',
            'controller' => 'news',
            'action'     => 'detail',
        ), array(
            '1' => 'slug',
        ),
        'tin-tuc/%s'
    ),

    'newsindex' => new Zend_Controller_Router_Route_Regex(
        'tin-tuc',
        array(
            'module'     => 'web',
            'controller' => 'news',
            'action'     => 'index',
        ), array(
        ),
        'tin-tuc'
    ),
    'newsintro' => new Zend_Controller_Router_Route_Regex(
        'gioi-thieu',
        array(
            'module'     => 'web',
            'controller' => 'news',
            'action'     => 'intro',
        ), array(
        ),
        'gioi-thieu'
    ),
    'newscontact' => new Zend_Controller_Router_Route_Regex(
        'lien-he',
        array(
            'module'     => 'web',
            'controller' => 'news',
            'action'     => 'contact',
        ), array(
        ),
        'lien-he'
    ),
    'redeemdetail' => new Zend_Controller_Router_Route_Regex(
        'tich-diem/([0-9]+)',
        array(
            'module'     => 'android',
            'controller' => 'redeem',
            'action'     => 'detail',
        ), array(
            '1' => 'id',
        ),
        'tich-diem/%d'
    ),
    'prizedetail' => new Zend_Controller_Router_Route_Regex(
        'phan-thuong/([0-9]+)',
        array(
            'module'     => 'android',
            'controller' => 'prize',
            'action'     => 'detail',
        ), array(
            '1' => 'pId',
        ),
        'phan-thuong/%d'
    ),
    'brandoffermap' => new Zend_Controller_Router_Route_Regex(
        'k-mai/duong-di/([0-9]+)-([0-9]+)',
        array(
            'module'     => 'android',
            'controller' => 'brand',
            'action'     => 'offermap',
        ), array(
            '1' => 'id',
            '2' => 'offerid',
        ),
        'k-mai/duong-di/%d-%d'
    ),
    'brandmenu' => new Zend_Controller_Router_Route_Regex(
        'menu/([0-9]+)-([0-9]+)',
        array(
            'module'     => 'android',
            'controller' => 'brand',
            'action'     => 'menu',
        ), array(
            '1' => 'id',
            '2' => 'oid',
        ),
        'menu/%d-%d'
    ),

);